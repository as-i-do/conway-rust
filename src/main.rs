extern crate terminal_size;
extern crate rand;
use rand::prelude::*;
use std::io::{self, Write};
use std::{thread, time};
use terminal_size::{Width, Height, terminal_size};


fn sleepnow(t: u64){
    let millis = time::Duration::from_millis(t);
    thread::sleep(millis)
}

/// # Basic CELL struct. 
///
/// Each cell is going to know it's X and Y address in the matrix so that it
/// can eventually use self.check_neighbours to determine the next state.
struct Cell {
    // Didn't know what type to make coords. Began with i32, then briefly used
    // usize... ended up with u16 (unsigned 16 bit) Should be big enough for a
    // matrix since realistically we shouldn't be doing more than 4k in either
    // direction. (also makes it easier to work with terminal size)
    xaddr: u16,
    yaddr: u16,

    // We're going to store state as a u8 -- this way we can make a cool
    // gradient effect
    life: u8,
}


impl Cell {
    /// Check the 8 surrounding cells for alive-ness
    fn check_neighbours(self: &Self, matrix: &Matrix) -> Cell {
        // Quck and dirty casting -- looks like we can just inline 'as' things
        // when we need to (assuming they're able to cast)
        let mwidth = matrix.cells[0].len() as u16 - 1;
        let mheight = matrix.cells.len() as u16 - 1;

        // I REALLY like the match syntax in rust so far.
        // The fact that all cases must be covered is a really nice.
        let left = match self.xaddr{
            0 => mwidth,
            _ => self.xaddr - 1

        } as usize;

        let right = match self.xaddr{
            // This one is a little more obscure -- we need to define a
            // variable before doing conditionals. This would allow "n"
            // to be used IN the case, theoretically. I guess it's a bit
            // like a lambda function.
            // NOTE: If you try to just do mwidth => 0, it will ALWAYS
            // match -- since mwidth would be redefined as
            // mwidth = self.xaddr.
            n if n == mwidth => 0,
            _                => self.xaddr + 1
        } as usize;

        let top = match self.yaddr{
            0 => mheight,
            _ => self.yaddr - 1
        } as usize;
        
        let bottom = match self.yaddr{
            n if n == mheight => 0,
            _ => self.yaddr + 1
        } as usize;
 
        // The next part determines where to look for neighbours
        let mut neighbours = 0;
        
        // This is how we define arrays in rust -- type definition on the left
        // and length after a semicolon. Not all of it is necessary if the
        // compiler can figure it out, though.
        let checks: [(usize, usize); 8] = [
            (left, self.yaddr as usize),
            (right, self.yaddr as usize),
            (left, top),
            (right, top),
            (self.xaddr as usize, top),
            (left, bottom),
            (right, bottom),
            (self.xaddr as usize, bottom)
        ];
        
        // Oh boy, probably lost an hour on this one. Not like python at all.
        // Not sure I even 100% get this one -- more the borrow checker
        // corrected me into submission.
        // checks needs to be borrowed as a reference so it's not consumed
        // however, for check in &checks returns a *reference* to an item in
        // checks. So, we use another ampersand, I think, to DEreference.
        for &check in &checks{
            let (x, y) = check;
            if matrix.cells[y][x].life == 255{
                neighbours = neighbours + 1;
            }
            if self.life == 255 && neighbours > 3 {
                break;
            }
        }
 
        let next = match self.life {
            255 => match neighbours {
                // I'm absolutely in LOVE with OR inside of a match.
                // This syntax is lovely.
                2|3 => 255,
                _   => 254
            },
            // Going to catch other cases -- we'll need to handle life = 0
            // as a special case.
            n   => match neighbours {
                3 => 255,
                _ => match n {
                    // Handle that life = 0 case!
                    0 => 0,
                    // Otherwise just decrease life for that gradient effect
                    _ => n - 1
                }
            },
        };
            
        Cell{
            xaddr: self.xaddr,
            yaddr: self.yaddr,
            life: next,
        }
     }

}


impl std::fmt::Display for Cell {
    // Alright, this ugly sunnova bitch lets you do printing. By default a 
    // struct has no nice way to display itself, and if you try to print it
    // you get shit all (won't even compile due to strict compiler). If you 
    // don't *need* to print in production, rust has a cool trait 
    // (decorator) called #\[derive(Debug)\] that will let you print your
    // struct like so: (println!("{:?}", x)). That's great, but for conway in
    // terminal we actually need to print, so we pull in the DISPLAY formatter.
    // The markup there is just what's needed, no sugar around that one.
    // NOTE: besides display there's other formatters like Binary (see docs).
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self.life {
            // For terminal output we're only going to do gradients for
            // 2 generations
            255 => write!(f, "O"),
            254 => write!(f, "o"),
            253 => write!(f, "."),
            _   => write!(f, " ")
        }
    }
}


/// A matrix that holds cells
struct Matrix {
    // Simple enough: we're going to have a vector of vectors of cells!
    // This gives us rows of cells, effectively.
    cells: Vec<Vec<Cell>>
}


impl Matrix {
    /// Constructs a row of random cells
    fn construct_row(width: u16, rowindex: u16) -> Vec<Cell> {
        // Vectors are just easily resizable arrays, it seems, and come in the
        // standard library.
        // They're a bit scary, however. It seems pointers break if you resize
        // the vector, since new memory has to be allocated.
        let mut new_row = Vec::new();
        for x in 0..width {
            new_row.push(
                Cell{
                    xaddr: x,
                    yaddr: rowindex,
                    life: match random() {
                        true  => 255,
                        false => 0,
                    },
                }
            );
        }
        // I'm not sure if I like the syntactic sugar here. No 'return'
        // statement needed -- just don't inlcude a semicolon.
        new_row
    }

    /// Constructs a Matrix of random cells
    fn construct(width: u16, height: u16) -> Matrix {
        let mut v = Vec::new();
        for x in 0..height{
            v.push(Matrix::construct_row(width, x));
        }
        Matrix{cells: v}
    }

    /// Runs each cells' neighbour checking logic
    fn rebuild_row(self: &Self, rowindex: u16) -> Vec<Cell> {
        let mut v = Vec::new();
        for item in &self.cells[(rowindex as usize)] {
            v.push(item.check_neighbours(self));
        }
        v
    }

    /// Runs the whole Matrix's rebuild logic and returns a new Matrix
    fn rebuild(self: &Self) -> Matrix {
        
        let mut v = Vec::new();
        for x in 0..(self.cells.len() as u16){
            v.push(self.rebuild_row(x));
        }
        Matrix{cells: v}
    }

    fn print(self: &Self) -> (){
        let mut output = String::new();
        for row in &self.cells{
            for item in row{
                // We're using print! not println! here.
                // print doesn't have a newline, meaning we're going to have
                // to do a stdout flush below to ensure all the stuff we're
                // 'print!ing' to the buffer actually shows up.
                output.push_str(&item.to_string());
            }
            // Two things here -- flush and unwrap.
            // the stdout().flush() pushes anything in the stdout buffer to
            // the terminal. Unwrap is me being lazy -- if flush encounters an
            // error we're just going to crash instead of dealing with it.
        }
        // Using some pretty scary terminal commands here I didn't
        // previously know existed.
        // \x1B[?25l hides the cursor
        // \x1B[?25h shows the cursor
        // \x1B[XA moves the cursor up X rows
        // I had to experiment a LOT to get this working nicely
        // without jitter or popping.
        print!("\x1B[?25l{}\x1B[?25h\n", output);
        print!("\x1B[{}A", &self.cells.len());
        // This ensures we've flushed stdout before continuing
        // (meaning everthings gone to the terminal)
        io::stdout().flush().unwrap();
    }
}


fn main() {
    let size =  terminal_size();
    let (w, h) = match size {
        // terminal_size returns an Option enum, which means we need a match
        // that matches either Some, or None. The Some it returns is
        // (Width, Height) (both defined by the package to be u16s)
        // So, we do a fancy deconstructor here which will effectively extract
        // our width and height into u16.
        Some((Width(w), Height(h))) => (w, h),
        // Default to 10x10 if we fail.
        None => (10, 10)
    };
    // We're going to go ONE line less than terminal height -- this is purely
    // empirical and the only way I got things working without being 'wiggly'
    let mut matrix = Matrix::construct(w, h - 1);
    loop {
        matrix = matrix.rebuild();
        matrix.print();
        // Not currently doing framerate properly. We'll do that later.
        sleepnow(30);
    }
}
